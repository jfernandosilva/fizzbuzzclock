import spock.lang.Specification
import spock.lang.Unroll

class FizzBuzzClockTest extends Specification {

    @Unroll("given #input hour must be #expectedHour and minutes must be #expectedMinutes")
    void 'should FizzBuzzClock store hour and minutes from a valid input'() {
        given:
        FizzBuzzClock fizzBuzzClockInstance = new FizzBuzzClock(time: input)

        expect:
        fizzBuzzClockInstance.hour == expectedHour
        fizzBuzzClockInstance.minutes == expectedMinutes

        where:
        input   | expectedHour | expectedMinutes
        "13:33" | 1            | 33
        "13:33" | 1            | 33
        "21:00" | 9            | 00
        "11:15" | 11           | 15
        "03:03" | 3            | 03
        "14:30" | 2            | 30
        "08:55" | 8            | 55
        "00:00" | 12           | 00
        "12:00" | 12           | 00
    }

    @Unroll
    void 'should raise exception when FizzBuzzClock receives a invalid input'() {
        when:
        new FizzBuzzClock(time: time)

        then:
        def error = thrown(expectedException)
        error.message == expectedMessage

        where:
        time     | expectedException  | expectedMessage
        "010110" | Exception          | 'invalid input'
        "qwerq"  | Exception          | 'invalid input'
        "111"    | Exception          | 'invalid input'
        "25:00"  | Exception          | 'invalid input'
        "26:60"  | Exception          | 'invalid input'
        "1:10"   | Exception          | 'invalid input'
        "10:1"   | Exception          | 'invalid input'
    }

    void 'should sound Fizz when a minute is evenly divisible by three'() {
        given:
        String time = "03:03"
        when:
        String sounds = new FizzBuzzClock(time: time).selectSoundsForTime()
        then:
        sounds == 'Fizz'
    }

    void 'should sound Buzz when a minute is evenly divisible by five'() {
        given:
        String time = "03:05"
        when:
        String sounds = new FizzBuzzClock(time: time).selectSoundsForTime()
        then:
        sounds == 'Buzz'
    }

    void 'should sound Fizz Buzz when a minute is evenly divisible by three and five'() {
        given:
        String time = "03:15"
        when:
        String sounds = new FizzBuzzClock(time: time).selectSoundsForTime()
        then:
        sounds == 'Fizz Buzz'
    }

    @Unroll
    void 'should not sound FizzBuzz when a minute is evenly divisible by three and five and is hour or half hour'() {
        expect:
        new FizzBuzzClock(time: time).selectSoundsForTime() != notExpectedSounds

        where:
        time    | notExpectedSounds
        "01:00" | "Fizz Buzz"
        "02:30" | "Fizz Buzz"
    }

    @Unroll
    void '''should sound Cuckoo n times until 12 depending the hour when is a hour with 0 minutes
        and minute is evenly divisible by three and five'''() {
        expect:
        new FizzBuzzClock(time: time).selectSoundsForTime() == expectedSounds

        where:
        time   || expectedSounds
        "01:00" || "Cuckoo"
        "02:00" || "Cuckoo Cuckoo"
        "03:00" || "Cuckoo Cuckoo Cuckoo"
        "04:00" || "Cuckoo Cuckoo Cuckoo Cuckoo"
        "05:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "06:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "07:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "08:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "09:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "10:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "11:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "12:00" || "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
    }

    void 'should sound Cuckoo one time when is half hour and minute is evenly divisible by three and five'() {
        given:
        String time = "01:30"
        when:
        String sounds = new FizzBuzzClock(time: time).selectSoundsForTime()
        then:
        sounds == 'Cuckoo'
   }

    void 'should sound tick when minute is not evenly divisible by three and five'() {
        given:
        String time = "01:37"
        when:
        String sounds = new FizzBuzzClock(time: time).selectSoundsForTime()
        then:
        sounds == 'tick'
    }

    @Unroll("given #input Fizz Buzz Clock sounds #expectedOutput")
    void 'test a lot of inputs'() {
        expect:
        new FizzBuzzClock(time: input).selectSoundsForTime() == expectedOutput

        where:
        input   | expectedOutput
        "13:33" | "tick"
        "21:00" | "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "11:15" | "Fizz Buzz"
        "03:03" | "Fizz"
        "14:30" | "Cuckoo"
        "08:55" | "Buzz"
        "00:00" | "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
        "12:00" | "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo"
    }
}