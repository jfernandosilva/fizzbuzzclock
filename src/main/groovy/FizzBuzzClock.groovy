import groovy.transform.CompileStatic

import java.time.LocalTime

@CompileStatic
class FizzBuzzClock {
    private String time
    private Long hour
    private Long minutes

    void setTime(String time) {
        this.time = time
        this.hour = getHourFromTime(time)
        this.minutes = parseTime(time).getMinute()
    }

    String selectSoundsForTime() {
        String sounds = ""
        if((this.minutes % 3 == 0) || (this.minutes % 5 == 0)) {
            sounds = "generateSoundsWhenMinutesAreDivisibleBy3and5"(this.hour, this.minutes)
        } else {
            sounds += "tick"
        }
        return sounds.trim()
    }

    private String "generateSoundsWhenMinutesAreDivisibleBy3and5"(Long hour, Long minutes) {
        String sounds = ""
        if(minutes != 0 && minutes != 30) {
            if(minutes % 3  == 0) {
                sounds += "Fizz "
            }
            if(minutes % 5 == 0) {
                sounds += "Buzz "
            }
        } else if(hour >= 1 && minutes == 0) {
            Math.min(12, hour).times {
                sounds += "Cuckoo "
            }
        } else if(minutes == 30) {
            sounds += "Cuckoo "
        }
        return sounds
    }

    private Integer getHourFromTime(String time) {
        LocalTime localtime = parseTime(time)
        Integer hour = localtime.hour
        if(hour == 0) {
            hour = 12
        } else if(hour > 12) {
            hour -= 12
        }
        return hour
    }

    private LocalTime parseTime(String time) {
        LocalTime localtime
        try {
            localtime = LocalTime.parse(time)
        } catch(Exception e) {
            throw new Exception('invalid input')
        }
        localtime
    }
}