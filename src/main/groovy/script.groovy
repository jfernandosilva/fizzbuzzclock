if(args) {
    try {
        FizzBuzzClock fizzBuzzClockTestInstance = new FizzBuzzClock(time: args[0])
        println "${args[0]} => ${fizzBuzzClockTestInstance.selectSoundsForTime()}"
    } catch(Exception e) {
        println e.message
    }

} else {
    println "invalid input"
}