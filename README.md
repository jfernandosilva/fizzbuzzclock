Código para resolver problema de
https://bitbucket.org/snippets/bookandlearn/8eM8oB/fizz-buzz-clock

El código fue hecho con groovy, las pruebas unitarias con spock y el proyecto fue construido con gradle.

Para ejecutar el script en una consola ejecutamos el siguiente comando.
En sistemas unix:
```sh
./gradlew runScript -Ptime=timeInput
```
En windows:
```sh
gradlew.bat runScript -Ptime=timeInput
```

Donde timeInput es la hora para la cual necesitamos ejecutar la función generadora de sonidos.
Por ejemplo para las 13:00 hrs, el comando a ejecutar es el siguiente.

En unix:
```sh
./gradlew runScript -Ptime=13:00
```
En windows:
```sh
gradlew.bat runScript -Ptime=13:00
```

Para ejecutar las pruebas el comando es el siguiente:
En sistemas unix:
```sh
./gradlew test
```
En windows:
```sh
gradlew.bat test
```